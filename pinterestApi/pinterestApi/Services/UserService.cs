using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using pinterestApi.Models;
using pinterestApi.Helpers;


namespace pinterestApi.Services
{
    public interface IUserService
    {
        client Authenticate(string username, string password, List<client> _users );
    }

    public class UserService : IUserService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        //private List<client> _users = new List<client>
        //{ 
            //new client { id = 1, client_name = "Test", client_lastname = "User", username = "test", pass = "test" } 
        //};

        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public client Authenticate(string username, string password, List<client> _users)
        {
            
           
            var user = _users.SingleOrDefault(x => x.username == username && x.pass == password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    //new Claim(ClaimTypes.Name, user.username.ToString()),
                    new Claim(ClaimTypes.Actor, user.id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            var tok = tokenHandler.ReadToken(user.Token);
               
            //user.Token = tokenHandler.WriteToken(tok);
            // remove password before returning
            user.pass= "";
            


            return user;
        }
       
    }
}