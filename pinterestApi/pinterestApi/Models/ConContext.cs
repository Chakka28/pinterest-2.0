﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pinterestApi.Models
{
    public class ConContext : DbContext
    {
        public ConContext(DbContextOptions<ConContext> options)
        : base(options)
        {
        }
        public DbSet<client> client { get; set; }
        public DbSet<imagen> imagen { get; set; }
        public DbSet<tag> tag { get; set; }
        
        public DbSet<clientxtag> clientxtag { get; set; }
        public DbSet<imgxtag> imgxtag { get; set; }

    }
}
