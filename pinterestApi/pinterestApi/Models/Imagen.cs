﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pinterestApi.Models
{
    public class imagen
    {
        public int id { get; set; }
        public string description_image { get; set; }
        public string url_image { get; set; }
        public string State_image { get; set; }
        public int idcliente { get; set; }
        public bool active { get; set; }
    }
}
