﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace pinterestApi.Models
{
    public class client
    {
        public int id { get; set; }
        public string client_name { get; set; }
        public string client_lastname { get; set; }
        public bool incognito { get; set; }
        public string username { get; set; }
        public string pass { get; set; }
        public bool active { get; set; }
        public string Token { get; set; }
    }
}
