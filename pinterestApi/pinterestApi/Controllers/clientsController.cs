﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pinterestApi.Models;
using pinterestApi.Services;
using System.Text;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

using System.IdentityModel.Tokens.Jwt;

namespace pinterestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class clientsController : ControllerBase
    {
        private readonly ConContext _context;
        private IUserService _userService;

        public clientsController(ConContext context, IUserService userService)
        {
            _context = context;
            _userService = userService;
        }

        // GET: api/clients
        [HttpGet]
        public async Task<ActionResult<IEnumerable<client>>> Getclient()
        {
            return await _context.client.ToListAsync();
        }

        // GET: api/clients/5
        [HttpGet("{id}")]
        public async Task<ActionResult<client>> Getclient(long id)
        {
            var client = await _context.client.FindAsync(id);

            if (client == null)
            {
                return NotFound();
            }

            return client;
        }

        // PUT: api/clients/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putclient(long id, client client)
        {
            if (id != client.id)
            {
                return BadRequest();
            }

            _context.Entry(client).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!clientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // PUT: api/clients/update/5
        [HttpPut("update/{id}")]
        public async Task<IActionResult> UpdatePutclient(int id, client clientupdate)
        {
            if (id != clientupdate.id)
            {
                return BadRequest();
            }
            var client = await _context.client.FindAsync(id);

            if (client == null)
            {
                return BadRequest();
            }
            client.incognito = clientupdate.incognito;

            _context.Entry(client).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!clientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }



        // POST: api/clients
        [HttpPost]
        public async Task<ActionResult<client>> Postclient(client client)
        {   
            int codeerror = CheckData(client);
            if(codeerror > 0)
                return BadRequest(new { message = codeerror });

            
            string old = client.pass;
            client.pass = Hash(old);
            _context.client.Add(client);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getclient", new { id = client.id }, client);
        }

        // DELETE: api/clients/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<client>> Deleteclient(long id)
        {
            var client = await _context.client.FindAsync(id);
            if (client == null)
            {
                return NotFound();
            }

            _context.client.Remove(client);
            await _context.SaveChangesAsync();

            return client;
        }

       



        private bool clientExists(long id)
        {
            return _context.client.Any(e => e.id == id);
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> AuthenticateAsync([FromBody]client userParam)
        {   
            if(userParam.pass == "" || userParam.username == "")
                return BadRequest(new { message = "Los datos no son validos" });
            
            
            string old = userParam.pass;
            userParam.pass = Hash(old);
            
            List<client> _users = await _context.client.ToListAsync();;
            var user = _userService.Authenticate(userParam.username, userParam.pass,_users);

            if (user == null)
                return BadRequest(new { message = "Los datos no son validos" });

            return Ok(user);
        }
        
      
        [HttpGet("checkauthentication/{id}")] 
        public async Task<ActionResult<client>> checkauthentication(int id)
        {   
            //var tokenHandler = new JwtSecurityTokenHandler();
            //var tok = tokenHandler.ReadToken(token);
           
            var c = await _context.client.FindAsync(id);

            if (c == null)
            {
                return BadRequest(new { message = "No se encontro" });

            }

            return c;
        }


        public string Hash  (string old){
            byte[] salt = new byte[128];
            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
            password: old,
            salt: salt,
            prf: KeyDerivationPrf.HMACSHA1,
            iterationCount: 10000,
            numBytesRequested: 256 / 8));

            return hashed;
        }

        public int  CheckData(client client)
        {
            if(client.client_name == "" || client.client_name == null){
                return 1;
            }
            if(client.client_lastname == "" || client.client_lastname == null){
                return 2;
            }
            if(client.username == "" || client.username == null){
                return 3;
            }
            if(client.pass == "" || client.pass == null){
                return 4;
            }
            else{
                return 0;
            }



        }
        
    }
}
