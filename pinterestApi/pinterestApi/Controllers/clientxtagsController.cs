﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pinterestApi.Models;

namespace pinterestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class clientxtagsController : ControllerBase
    {
        private readonly ConContext _context;

        public clientxtagsController(ConContext context)
        {
            _context = context;
        }

        // GET: api/clientxtags
        [HttpGet]
        public async Task<ActionResult<IEnumerable<clientxtag>>> Getclientxtag()
        {
            return await _context.clientxtag.ToListAsync();
        }

        // GET: api/clientxtags/5
        [HttpGet("{id}")]
        public async Task<ActionResult<clientxtag>> Getclientxtag(int id)
        {
            var clientxtag = await _context.clientxtag.FindAsync(id);

            if (clientxtag == null)
            {
                return NotFound();
            }

            return clientxtag;
        }

        // PUT: api/clientxtags/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putclientxtag(int id, clientxtag clientxtag)
        {
            if (id != clientxtag.id)
            {
                return BadRequest();
            }

            _context.Entry(clientxtag).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!clientxtagExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/clientxtags
        [HttpPost]
        public async Task<ActionResult<clientxtag>> Postclientxtag(clientxtag clientxtag)
        {
            _context.clientxtag.Add(clientxtag);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getclientxtag", new { id = clientxtag.id }, clientxtag);
        }

        // DELETE: api/clientxtags/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<clientxtag>> Deleteclientxtag(int id)
        {
            var clientxtag = await _context.clientxtag.FindAsync(id);
            if (clientxtag == null)
            {
                return NotFound();
            }

            _context.clientxtag.Remove(clientxtag);
            await _context.SaveChangesAsync();

            return clientxtag;
        }

        private bool clientxtagExists(int id)
        {
            return _context.clientxtag.Any(e => e.id == id);
        }
    }
}
