﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pinterestApi.Models;

namespace pinterestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class imagensController : ControllerBase
    {
        private readonly ConContext _context;

        public imagensController(ConContext context)
        {
            _context = context;
        }

        // GET: api/imagens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<imagen>>> Getimagen()
        {
            return await _context.imagen.ToListAsync();
        }

        // GET: api/imagens/5
        [HttpGet("{id}")]
        public async Task<ActionResult<imagen>> Getimagen(long id)
        {
            var imagen = await _context.imagen.FindAsync(id);

            if (imagen == null)
            {
                return NotFound();
            }

            return imagen;
        }

        // PUT: api/imagens/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putimagen(long id, imagen imagen)
        {
            if (id != imagen.id)
            {
                return BadRequest();
            }

            _context.Entry(imagen).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!imagenExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/imagens
        [HttpPost]
        public async Task<ActionResult<imagen>> Postimagen(imagen imagen)
        {
            _context.imagen.Add(imagen);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getimagen", new { id = imagen.id }, imagen);
        }

        // DELETE: api/imagens/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<imagen>> Deleteimagen(long id)
        {
            var imagen = await _context.imagen.FindAsync(id);
            if (imagen == null)
            {
                return NotFound();
            }

            _context.imagen.Remove(imagen);
            await _context.SaveChangesAsync();

            return imagen;
        }

        private bool imagenExists(long id)
        {
            return _context.imagen.Any(e => e.id == id);
        }
    }
}
