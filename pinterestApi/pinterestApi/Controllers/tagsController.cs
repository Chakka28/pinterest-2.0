﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pinterestApi.Models;

namespace pinterestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class tagsController : ControllerBase
    {
        private readonly ConContext _context;

        public tagsController(ConContext context)
        {
            _context = context;
        }

        // GET: api/tags
        [HttpGet]
        public async Task<ActionResult<IEnumerable<tag>>> Gettag()
        {
            return await _context.tag.ToListAsync();
        }

        // GET: api/tags/5
        [HttpGet("{id}")]
        public async Task<ActionResult<tag>> Gettag(long id)
        {
            var tag = await _context.tag.FindAsync(id);

            if (tag == null)
            {
                return NotFound();
            }

            return tag;
        }

        // PUT: api/tags/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Puttag(long id, tag tag)
        {
            if (id != tag.id)
            {
                return BadRequest();
            }

            _context.Entry(tag).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tagExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/tags
        [HttpPost]
        public async Task<ActionResult<tag>> Posttag(tag tag)
        {
            _context.tag.Add(tag);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Gettag", new { id = tag.id }, tag);
        }

        // DELETE: api/tags/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<tag>> Deletetag(long id)
        {
            var tag = await _context.tag.FindAsync(id);
            if (tag == null)
            {
                return NotFound();
            }

            _context.tag.Remove(tag);
            await _context.SaveChangesAsync();

            return tag;
        }

        private bool tagExists(long id)
        {
            return _context.tag.Any(e => e.id == id);
        }
    }
}
