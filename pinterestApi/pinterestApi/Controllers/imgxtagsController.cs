﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pinterestApi.Models;

namespace pinterestApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class imgxtagsController : ControllerBase
    {
        private readonly ConContext _context;

        public imgxtagsController(ConContext context)
        {
            _context = context;
        }

        // GET: api/imgxtags
        [HttpGet]
        public async Task<ActionResult<IEnumerable<imgxtag>>> Getimgxtag()
        {
            return await _context.imgxtag.ToListAsync();
        }

        // GET: api/imgxtags/5
        [HttpGet("{id}")]
        public async Task<ActionResult<imgxtag>> Getimgxtag(int id)
        {
            var imgxtag = await _context.imgxtag.FindAsync(id);

            if (imgxtag == null)
            {
                return NotFound();
            }

            return imgxtag;
        }

        // PUT: api/imgxtags/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putimgxtag(int id, imgxtag imgxtag)
        {
            if (id != imgxtag.id)
            {
                return BadRequest();
            }

            _context.Entry(imgxtag).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!imgxtagExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/imgxtags
        [HttpPost]
        public async Task<ActionResult<imgxtag>> Postimgxtag(imgxtag imgxtag)
        {
            _context.imgxtag.Add(imgxtag);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getimgxtag", new { id = imgxtag.id }, imgxtag);
        }

        // DELETE: api/imgxtags/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<imgxtag>> Deleteimgxtag(int id)
        {
            var imgxtag = await _context.imgxtag.FindAsync(id);
            if (imgxtag == null)
            {
                return NotFound();
            }

            _context.imgxtag.Remove(imgxtag);
            await _context.SaveChangesAsync();

            return imgxtag;
        }

        private bool imgxtagExists(int id)
        {
            return _context.imgxtag.Any(e => e.id == id);
        }
    }
}
