export class Client {
     id: number;
     client_name: string;
     client_lastname:string;
     incognito: boolean; 
     username: string; 
     pass: string; 
     active: boolean;
     repass: string;
     token: string;
}
