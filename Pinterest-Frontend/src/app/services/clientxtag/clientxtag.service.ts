import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Clientxtag } from 'src/app/models/clientxtag';

@Injectable({
  providedIn: 'root'
})
export class ClientxtagService {
  API_URI = 'http://localhost:5000/api/clientxtags';

  constructor(private http: HttpClient) { }

  Addclientxtag(clientxtag:Clientxtag){
    return this.http.post(`${this.API_URI}`,clientxtag);
  }

  Getclientxtag(){
    return this.http.get(`${this.API_URI}`);
  }
}
