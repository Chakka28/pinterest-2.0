import { TestBed } from '@angular/core/testing';

import { ClientxtagService } from './clientxtag.service';

describe('ClientxtagService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientxtagService = TestBed.get(ClientxtagService);
    expect(service).toBeTruthy();
  });
});
