import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Imgxtag } from 'src/app/models/imgxtag';

@Injectable({
  providedIn: 'root'
})
export class ImgxtagService {
  API_URI = 'http://localhost:5000/api/imgxtags';

  constructor(private http: HttpClient) { }

  Addtagximg(imgxtag:Imgxtag){
    return this.http.post(`${this.API_URI}`,imgxtag);

  }

  get(){
    return this.http.get(`${this.API_URI}`);
  }
}
