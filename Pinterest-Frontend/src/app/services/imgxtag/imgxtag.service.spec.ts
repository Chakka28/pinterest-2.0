import { TestBed } from '@angular/core/testing';

import { ImgxtagService } from './imgxtag.service';

describe('ImgxtagService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImgxtagService = TestBed.get(ImgxtagService);
    expect(service).toBeTruthy();
  });
});
