import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Client } from "../../models/client";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  API_URI = 'https://localhost:5001/api/clients';

  constructor(private http: HttpClient) { }

  AddClient(client : Client){
    return this.http.post(`${this.API_URI}`,client);
  }

  UpdateClient(client: Client){
    return this.http.put(`${this.API_URI}/update/${client.id}`,client);
  }

}
