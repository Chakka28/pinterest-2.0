import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Imagen } from "../../models/imagen";

@Injectable({
  providedIn: 'root'
})
export class ImagenService {
  API_URI = 'https://localhost:5001/api/imagens';

  constructor(private http: HttpClient ) { }

  AddImagen(imagen : Imagen){
    return this.http.post(`${this.API_URI}`,imagen);

  }

  getImage(){
    return this.http.get(`${this.API_URI}`);
  }
  
}
