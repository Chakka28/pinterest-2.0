import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { User } from "../models/user";
import { Client } from "../models/client";




@Injectable({
  providedIn: 'root'
})
export class UsersService {

  API_URI = 'https://localhost:5001/api/clients';

  constructor(private http: HttpClient) { }

  //Verifica si el usuario existe y si es así retornar un cliente con jwt
  checkUser(user : User){
    return this.http.post(`${this.API_URI}/authenticate`,user);
  }

  checkToken(client : Client){
    
    return this.http.get(`${this.API_URI}/checkauthentication/${client.id}` );
    
  }



}
