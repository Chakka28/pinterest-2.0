import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from "../app/components/index/index.component";
import { RegisterComponent } from "../app/components/register/register.component";
import { UploadComponent } from './components/upload/upload.component';
import { NavigationComponent } from "./components/navigation/navigation.component";
import { MainComponent } from "./components/main/main.component";

const routes: Routes = [
  {
    path: '',
    redirectTo:'/index',
    pathMatch: 'full'
  },
  {path:'index', component: IndexComponent},
  { path:'register', component: RegisterComponent},
  { path:'upload', component: UploadComponent},
  { path:'navigation', component:NavigationComponent},
  {path: 'main', component:MainComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
