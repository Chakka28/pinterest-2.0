import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Client } from "../../models/client";
import { ClientService } from "../../services/client/client.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


   client : Client={
     id :0,
     client_name :"",
     client_lastname : "",
     incognito : false,
     username : "",
     pass : "",
     active: true,
     repass : "",
     token: ""
     };
 
   errornombre = "";
   errorapellido = "";
   erroruser = "";
   errorpass = "";
   errorconfpass="";


  constructor(private clientservice: ClientService, private router: Router) { }
  
  ngOnInit() {
    
  }

  CheckData(client : Client){
    if(this.CheckNombre()){
      if(this.CheckApellido()){
        if(this.CheckUsername()){
          if(this.CheckPass()){
            if(this.CheckRepass()){
              if(this.CheckSamePass()){
                  this.client.repass = "";
                  this.clientservice.AddClient(this.client).subscribe(
                    res=>{
                      //console.log(res);
                      this.router.navigate(['/index']);
                    },
                    err => this.ShowError(err.error["message"])
                  )

              }
            }
          }
        }
      }
    }
  }

  CheckNombre(){
    if(this.client.client_name == ""){
      this.errornombre = "El campo de nombre no puede estar vacío";
      return false;
    }else{
      this.errornombre = "";
      return true;
    }
  }
  CheckApellido(){
    if(this.client.client_lastname == ""){
      this.errorapellido = "El campo de apellido no puede estar vacío";
      return false;
    }else{
      this.errorapellido = "";
      return true;
    }
  }
  CheckUsername(){
    if(this.client.username == ""){
      this.erroruser = "El campo de usuario no puede estar vacío";
      return false;
    }
    else{
      this.erroruser= "";
      return true;
    }
  }
  CheckPass(){
    if(this.client.pass == ""){
      this.errorpass = "El campo de contraseña no puede estar vacío";
      return false;
    }
    else{
      this.errorpass = "";
      return true;
    }
  }
  CheckRepass(){
    if(this.client.repass == ""){
      this.errorconfpass = "Debe de confirmar su contraseña";
      return false;
    }
    else{
      this.errorconfpass = "";
      return true;
    }
  }
  CheckSamePass(){
    if(this.client.pass != this.client.repass){
      this.errorpass = "Las contraseñas no coinciden";
      return false;
    }
    else{
      this.errorpass="";
      return true;
    }
  }
  ShowError(errorcode){
    switch (errorcode) {
      case 1:
          this.errornombre = "El campo de nombre no puede estar vacío";
        break;
      case 2:
          this.errorapellido = "El campo de apellido no puede estar vacío";
        break;
      case 3:
          this.erroruser = "El campo de usuario no puede estar vacío";
        break;
      case 4:
          this.errorpass = "El campo de contraseña no puede estar vacío";
        break;
      default:
        break;
    }

  }

}
