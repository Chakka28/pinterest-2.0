import { Component, OnInit } from '@angular/core';
import { ImagenService } from "../../services/imagen/imagen.service";
import { ImgxtagService } from "../../services/imgxtag/imgxtag.service";
import { ClientxtagService} from "../../services/clientxtag/clientxtag.service";
import { Imgxtag } from 'src/app/models/imgxtag';
import { Clientxtag } from 'src/app/models/clientxtag';
import {Imagen} from '../../models/imagen';
import { Router } from "@angular/router";
import { from } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {


  //variable de clientxtag
  private clientxtag: Clientxtag = {
    id : 0,
    idcliente: Number(localStorage.getItem("id")),
    tag : ""
  }

  // variable de imgxtag que se utiliza para filtro
  private filtro1 :Imgxtag = {
    id : 0,
    idimgen:0,
    tag:""
  };
  // lista de imagenes
  private imagenes;
  // lista usada para mant de img
  private fixedimagenes: any = new Array();
  //lista de clientes tag
  private clienttags:  any = new Array();
  // lista de tags
  private tag;
  // lista de client img
  private clientxImg;



  constructor(private ImagenService: ImagenService,private ImgxtagService: ImgxtagService, private Clientxtagservice:ClientxtagService, private router: Router) { }

  ngOnInit() {
    this.extraerImg();
    this.extrartag();
  }

  // metodo para extraer img
  extraerImg(){
    this.ImagenService.getImage().subscribe(
      res=>{
        this.imagenes = res;
        this.mostrar();
        this.verificarEstadoImg();
      },err => this.ShowError(err.error["message"])
      )
  }


// metodo para verificar estao de img
  verificarEstadoImg(){
    for(let itemimg of this.imagenes){
      if(itemimg.state_image == "Privada"){
        if(itemimg.idcliente != (localStorage.getItem("id"))){
          var index = this.imagenes.indexOf(itemimg);
          this.imagenes.splice(index,1)
        }
      }
    }
  }

  ShowError(arg0: any): void {
    throw new Error("Method not implemented.");
  }

  // metodo de extraer tag
  extrartag(){
    this.ImgxtagService.get().subscribe(
      res=>{
        this.tag = res;
      },err => this.ShowError(err.error["message"])
      )
  }

  verificarusuario(){
    if(localStorage.getItem("id") == null || localStorage.getItem("token") == null ||
     localStorage.getItem("incognito") == null){
      this.router.navigate(['/index']);
    }
  }
//Metodo para el filtro de img
  filtro(){
    this.verificarusuario();
    var mant =  new Array(); 
    for(let itemtag of this.tag){
      if(itemtag.tag == this.filtro1.tag){
        var idimage = itemtag.idimgen;
        for(let itemimg of this.imagenes){
          if(itemimg.id == idimage){
            this.imagenes[this.imagenes.indexOf(itemimg)] = "nulo";
            mant.push(itemimg);
          }
        }
      }
    }
    this.verificarlista(mant);
  }

  // verifica la lista img para borrar elementos innecesarios
verificarlista(lista){
  for(let itemimg of this.imagenes){
    if(itemimg != "nulo"){
      lista.push(itemimg);
    }
  }

  this.imagenes = lista;
}
 
// metodo para guardar los tags que marque le cliente
  guardartagxclient(idimg){
    this.verificarusuario();
    var modoIncognito = (localStorage.getItem("incognito"));

    if(modoIncognito == "false"){
      for(let item of this.tag){
        if(item.idimgen == idimg){
          this.clientxtag.tag = item.tag;
          this.Clientxtagservice.Addclientxtag(this.clientxtag).subscribe(
            res=>{
            },err => this.ShowError(err.error["message"])
            )
        }
      }
      this.ImagenesDinamicas(idimg);

    }else{
      console.log("si esta en modo incognito")
    }
  }

  // metodo donde verifica los ultimos tags del cliente y acomoda  las img
  mostrar(){
    this.verificarusuario();
    if(localStorage.getItem("id") == null){
      this.router.navigate(['/index']);
    }else{
      var tagsdecliente =  new Array(); 
      this.Clientxtagservice.Getclientxtag().subscribe(
        res=>{
          this.clienttags = res;
          for(let item of this.clienttags){
            if(item.idcliente == localStorage.getItem("id")){
              tagsdecliente.push(item);
            }
          }
          for(let item2 of this.tag){
            if(item2.tag == tagsdecliente[tagsdecliente.length-1].tag){
              for(let item3 of this.imagenes){
                if(item3.id == item2.idimgen){
                  this.fixedimagenes.push(item3);
                  this.imagenes[this.imagenes.indexOf(item3)] = "nulo";
                }
              }
            }
            if(item2.tag == tagsdecliente[tagsdecliente.length-2].tag){
              for(let item3 of this.imagenes){
                if(item3.id == item2.idimgen){
                  this.fixedimagenes.push(item3);
                  this.imagenes[this.imagenes.indexOf(item3)] = "nulo";
                }
              }
            }
            if(item2.tag == tagsdecliente[tagsdecliente.length-3].tag){
              for(let item3 of this.imagenes){
                if(item3.id == item2.idimgen){
                  this.fixedimagenes.push(item3);
                  this.imagenes[this.imagenes.indexOf(item3)] = "nulo";
                }
              }
            }
          }
          this.verificarlista(this.fixedimagenes)
          },err => this.ShowError(err.error["message"])
        )
      }
  }

  // realiza el procedimiento dinamico de img
  ImagenesDinamicas(idImg){
    this.verificarusuario();
    var arraymantenimiento =  new Array(); 
    var index = 0;
    var newtag = "";
    for(let itemImg of this.imagenes){
      if(itemImg.id == idImg){
        index = this.imagenes.indexOf(itemImg);
        arraymantenimiento.push(itemImg);
        for(let itemTag of this.tag){
            if(itemTag.idimgen == idImg){
              newtag = itemTag.tag;
            }
        }
        this.imagenes[this.imagenes.indexOf(itemImg)] = "nulo";
        break;
      }else{
        this.imagenes[this.imagenes.indexOf(itemImg)] = "nulo";
        arraymantenimiento.push(itemImg);
      }
    }
    for(let itemimg of this.imagenes)
      {     
          for(let itemTag of this.tag){
            if(itemTag.idimgen == itemimg.id){
              if(itemTag.tag == newtag){
                this.imagenes[this.imagenes.indexOf(itemimg)] = "nulo";
                arraymantenimiento.push(itemimg);
              }
            }
        }
      }
      this.verificarlista(arraymantenimiento);
    }
  }





