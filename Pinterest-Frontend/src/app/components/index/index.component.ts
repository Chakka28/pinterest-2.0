import { Component, OnInit } from '@angular/core';
import { User } from "../../models/User";
import { Router } from "@angular/router";
import { UsersService } from "../../services/users.service";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  
  user : User={
    username : '',
    pass : '',
 
  };

  erroruser = "";
  errorpass = "";
  errorlogin = "";

  constructor(private userservice: UsersService, private router: Router) { }

  ngOnInit() {
  }

  CheckUser(){
    if(this.CheckUsername()){
      if(this.CheckPass()){
          this.userservice.checkUser(this.user)
          .subscribe(
            res=>{
              console.log(res);
              localStorage.setItem('token', res['token']);
              localStorage.setItem('id', res['id']);
              localStorage.setItem('incognito', res['incognito']);
              this.router.navigate(['/main']);
            },
            err => this.errorlogin = err.error["message"]
        )
      }
    }
  }

  CheckUsername(){
    if(this.user.username == ""){
      this.erroruser = "El campo de usuario no puede estar vacío";
      return false;
    }
    else{
      this.erroruser= "";
      return true;
    }
  }
  CheckPass(){
    if(this.user.pass == ""){
      this.errorpass = "El campo de contraseña no puede estar vacío";
      return false;
    }
    else{
      this.errorpass = "";
      return true;
    }
  }

}
