import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpEventType, HttpClient } from '@angular/common/http';
import { Imagen } from "../../models/imagen";
import { ImagenService } from "../../services/imagen/imagen.service";
import { ImgxtagService } from "../../services/imgxtag/imgxtag.service";
import { Router } from '@angular/router';
import { TouchSequence } from 'selenium-webdriver';
import { Imgxtag } from 'src/app/models/imgxtag';
 
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  private tags =  new Array();
  private newfile ;
  private imageUrl: string = "/assets/img/cargar.jpg";
  private fileToUpload: File = null;


  private varimxtag : Imgxtag={
    id:0,
    idimgen:0,
    tag:""
    };

    private imagen : Imagen={
    id :0,
    description_image :"",
    url_image : "",
    State_image : "Publica",
    idcliente : Number(localStorage.getItem("id")),
    active: true,
    };

    private CheckDescripcion = "";
    private listerror = "";
    private fileerror = "";

    private newtask = {
      name :""
    };
  
  public progress: number;
  public message: string;

  @Output() public onUploadFinished = new EventEmitter();
 
  constructor(private http: HttpClient, private router: Router, private ImagenService: ImagenService,private ImgxtagService: ImgxtagService) { }
 
  ngOnInit() {

    //this.tags.push("madera");
    //this.tags.push("silla");
  }

  add(){
    if(this.newtask.name != ""){
      this.tags.push(this.newtask.name);
    }
  }

  delete(ntag){
    var index = this.tags.indexOf(ntag);
    if (index >= -1) {
      // modifies array in place
      this.tags.splice(index, 1);
    }
  	
  }


  onChange(centroId) {
    console.log(centroId); 
    if(centroId == 1){
      this.imagen.State_image = "publica";
    }else{
      this.imagen.State_image = "Privada";
    }
}




  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    //Show image preview
    var reader = new FileReader();
    reader.onload = (event:any) => {
      this.imageUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }
 
  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }
 
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
 
    this.http.post('https://localhost:5001/api/upload', formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.message = 'Upload success.';
          this.onUploadFinished.emit(event.body);
        }
      });
  }

  CheckData(imagen : Imagen){
    if(this.CheckDes()){
      if(this.Checklisttag()){
        if(this.Checkfile()){
          this.imagen.url_image = this.fileToUpload.name;
          this.ImagenService.AddImagen(this.imagen).subscribe(
          res=>{
            this.newfile = res;
            for(let tag of this.tags){
              this.varimxtag.idimgen = this.newfile.id;
              this.varimxtag.tag = tag;
              this.ImgxtagService.Addtagximg(this.varimxtag).subscribe(
                res=>{
                  this.router.navigate(['/main']);
                },err => this.ShowError(err.error["message"])
                )
              
            }
            
          },
          err => this.ShowError(err.error["message"])
          )

          
        }
        
      }

    }
    
  }
  ShowError(arg0: any): void {
    throw new Error("Method not implemented.");
  }

  CheckDes(){
    if(this.imagen.description_image == ""){
      this.CheckDescripcion = "El campo de nombre no puede estar vacío";
      return false;
    }else{
      this.CheckDescripcion = "";
      return true;
    }
  }

  Checklisttag(){
    if(this.tags.length == 0){
      this.listerror = "Necesita min 1 tag";
      return false;
    }else{
      this.listerror = "";
      return true;
    }
  }

  Checkfile(){
    if(this.fileToUpload == null){
      this.fileerror = "Agregue una imagen";
      return false;
    }else{
      this.fileerror = "";
      return true;
    }
  }

  

  }
  

  


 
