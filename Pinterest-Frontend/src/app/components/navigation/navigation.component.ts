import { Component, OnInit } from '@angular/core';
import { UsersService } from "../../services/users.service";
import { ClientService } from "../../services/client/client.service";
import { Router } from "@angular/router";
import { Client } from "../../models/client";
import * as jwt_decode from "jwt-decode";
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  
  client : Client={
    id :0,
    client_name :"",
    client_lastname : "",
    incognito : false,
    username : "",
    pass : "",
    active: true,
    repass : "",
    token: ""
    };

    incognitoactive = {is_active : false};



  constructor(private userservice: UsersService, private router: Router, private clientservice : ClientService) { }

  ngOnInit() {
    this.CheckUserToken();
    this.CheckMode();
  }

  CheckUserToken(){
    var T = localStorage.getItem('token');
    if(T == null){
      this.router.navigate(['/index'])
    }
    else{
      var code =  this.getDecodedAccessToken(T);
      this.client.id = code['actort'];
      this.userservice.checkToken(this.client)
       .subscribe(
         res=>{
  
         },
        err => this.router.navigate(['/index'])
     ) 

    }
  }

  logout(){

    if(localStorage.getItem('incognito') == "true"){
      this.client.incognito = true;
      this.client.id = parseInt(localStorage.getItem('id'));
      this.clientservice.UpdateClient(this.client)
      .subscribe(
        res=>{
          console.log(res);
 
        },
       err => console.log(err) )

    }
    else{
      this.client.incognito = false;
      this.client.id = parseInt(localStorage.getItem('id'));
      this.clientservice.UpdateClient(this.client)
      .subscribe(
        res=>{
 
        },
       err => console.log(err)

    ) 
    }
    localStorage.removeItem('id');
    localStorage.removeItem('token');
    localStorage.removeItem('incognito');
    this.router.navigate(['/index']);

  }


  getDecodedAccessToken(token: string): any {
    try{
        return jwt_decode(token);
    }
    catch(Error){
        return null;
    }
  }
  CheckMode(){
    if(localStorage.getItem('incognito') == "true"){
     this.incognitoactive.is_active = true;
    }
    else{
      this.incognitoactive.is_active = false;
    }
  }

  SwitchMode(){
    if(localStorage.getItem('incognito') == "true"){
      localStorage.removeItem('incognito');
      localStorage.setItem('incognito', 'false');
      this.incognitoactive.is_active = false;
     }
     else{
      localStorage.removeItem('incognito');
      localStorage.setItem('incognito', 'true');
       this.incognitoactive.is_active = true;
     }

  }


}
